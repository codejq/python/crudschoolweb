from django.contrib import admin
from django.urls import path
from . import views
from .views import add_student

urlpatterns = [
    path('admin/', admin.site.urls),
    path('estudiantes/', views.lista_estudiantes, name='estudiantes'),  # Use view function without ()
    path('cursos/', views.lista_cursos, name='cursos'),  # Use view function without ()
    path('profesores/', views.lista_profesores, name='profesores'),  # Use view function without ()
    path('add_student/', add_student, name='add_student'),
]
