from django.shortcuts import render, redirect
from .models import Estudiante, Profesor, Curso
# Create your views here.


def lista_estudiantes(request):
    estudiantes = Estudiante.objects.all()
    return render(request, "lista_estudiantes.html", { 'estudiantes': estudiantes})

def lista_profesores(request):
    profesores = Profesor.objects.all()
    return render(request, "lista_profesores.html", {'profesores': profesores})

def lista_cursos(request):
    cursos = Curso.objects.all()
    return render(request, "lista_cursos.html", {'cursos': cursos})




def add_student(request):
    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        apellido = request.POST.get('apellido')
        edad = request.POST.get('edad')
        Estudiante.objects.create(nombre=nombre, apellido=apellido, edad=edad)
        return redirect('estudiantes')  # Redirect to the estudiantes page
    return render(request, 'lista_estudiantes.html')  # Render the form template for GET requests
