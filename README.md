# CRUD School Web Application

## Overview
This project is a web application developed in Python using Django framework. The Python version used is 3.12.2, and the project was developed using PyCharm IDE (Community Edition).

## Description
The application is a CRUD (Create, Read, Update, Delete) system for managing schools. It includes entities for students, courses, teachers, and assignments.

## Running the Application
To run the application, navigate to the `.\CRUDSchool\` directory and execute the following command:

python manage.py runserver

This will start the Django development server, and you can access the application through a web browser.

## Requirements
- Python 3.12.2
- PyCharm IDE (Community Edition)

## License
This project is licensed under the [MIT License](LICENSE).
